export async function getArrivalTimes(stop: string) {
    const response = await fetch('https://idfm-prim-proxy.ludchieng.fr/stop-monitoring?MonitoringRef=STIF:StopPoint:Q:463245:')
    return (await response.json()).Siri.ServiceDelivery.StopMonitoringDelivery[0].MonitoredStopVisit.map((visit) => new Date(visit.MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime).toLocaleTimeString())
}
